FROM node:12.16.0

WORKDIR /app/

COPY . .

RUN npm install

EXPOSE 3001

CMD [ "npm", "start" ]
