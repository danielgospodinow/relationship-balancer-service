const express = require("express");
const app = express();
const cors = require('cors');

const port = 3001;

app.use(cors());
app.listen(port, () => {
    console.log(`Server started at port ${port}`);
});

let counter = 0;

app.get("/status", (req, res, next) => {
    res.json({
        currentState: counter
    });
});

app.get("/increase", (req, res, next) => {
    counter += 1;

    res.json({
        currentState: counter
    });
});

app.get("/decrease", (req, res, next) => {
    counter -= 1;
    
    res.json({
        currentState: counter
    });
});
